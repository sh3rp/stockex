package org.kndl.stockex

import akka.actor.{Props, ActorSystem, Actor}


object Exchange extends App {
  val system = ActorSystem("Exchange")

  val exchange = system.actorOf(Props[ExchangeHandler],"exchange")

  val broker = system.actorOf(Props[Broker],"broker")

}
