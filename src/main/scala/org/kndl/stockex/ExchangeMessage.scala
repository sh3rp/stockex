package org.kndl.stockex

import java.util.UUID
import org.kndl.stockex.model.Stock

sealed trait ExchangeMessage {
  implicit val id: UUID = UUID.randomUUID()
  val timestamp: Long = System.currentTimeMillis()
}

case object Sync extends ExchangeMessage
case object Get_Bids extends ExchangeMessage
case object Get_Offers extends ExchangeMessage

case class Bid(brokerId: String, stock: Stock) extends ExchangeMessage
case class Offer(brokerId: String, stock: Stock) extends ExchangeMessage
case class Bid_Confirm(stock: Stock) extends ExchangeMessage
case class Offer_Confirm(stock: Stock) extends ExchangeMessage
