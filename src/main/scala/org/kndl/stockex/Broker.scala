package org.kndl.stockex

import org.kndl.stockex.model.Stock
import akka.actor.{Props, ActorRef, ActorSystem}

class Broker extends App {
  private var positions: Seq[Stock] = Seq()

  private val broker: BrokerHandler = new BrokerHandler("gm")

  def send(stock:Stock,bid:Boolean) {
    broker.send(stock,bid)
  }
}
