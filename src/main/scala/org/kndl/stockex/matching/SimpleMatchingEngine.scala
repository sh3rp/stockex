package org.kndl.stockex.matching

import org.kndl.stockex.model.{Transaction, Stock}

class SimpleMatchingEngine(pool:Seq[Stock]) extends MatchingEngine {
  def buy(stock: Stock): Transaction = ???

  def sell(stock: Stock): Transaction = ???
}
