package org.kndl.stockex.matching

import org.kndl.stockex.model.{Transaction, Stock}

trait MatchingEngine {
  def buy(stock: Stock): Transaction
  def sell(stock: Stock): Transaction
}
