package org.kndl.stockex

import akka.actor.{ActorRef, Props, ActorSystem, Actor}
import org.kndl.stockex.model.Stock

class BrokerHandler(id: String) extends Actor {
  def receive = {
    case Bid_Confirm(stock) => println("Received bid  : " + stock)
    case Offer_Confirm(stock) => println("Received offer: " + stock)
  }

  def send(stock: Stock, bid:Boolean): Unit = {
    val system = ActorSystem("exchange")
    val exchange = system.actorOf(Props[ExchangeHandler],"exchange")
    bid match {
      case true => exchange.tell(Bid(id,stock),ActorRef.noSender)
      case false => exchange.tell(Offer(id,stock),ActorRef.noSender)
    }
  }

}
