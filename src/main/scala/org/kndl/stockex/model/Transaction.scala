package org.kndl.stockex.model

case class Transaction(stock: Stock, buyer: String, seller: String, timestamp: Long = System.currentTimeMillis())
