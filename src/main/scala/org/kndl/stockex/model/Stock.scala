package org.kndl.stockex.model

case class Stock(symbol:String, shares: Long, paid: Double)
