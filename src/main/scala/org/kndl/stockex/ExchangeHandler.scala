package org.kndl.stockex

import akka.actor.{ActorRef, Actor}
import org.kndl.stockex.matching.{MatchingEngine, SimpleMatchingEngine}
import org.kndl.stockex.model.Stock

class ExchangeHandler extends Actor {

  def pool:Seq[Stock] = Seq()

  def matcher:MatchingEngine = new SimpleMatchingEngine(pool)

  def receive: Actor.Receive = {

    case Sync => {
    }

    case Bid(stock) => if(handleBid(stock)) sender ! Bid_Confirm(stock)

    case Offer(stock) => if(handleOffer(stock)) sender ! Offer_Confirm(stock)

  }

  def handleBid(stock: Stock): Boolean = {

    true
  }

  def handleOffer(stock: Stock): Boolean = {

    true
  }

}
