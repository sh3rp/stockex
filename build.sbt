name := "stockex"

version := "0.1"

organization := "org.kndl"

scalaVersion := "2.10.3"

libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-actor" % "2.3.2",
    "com.typesafe.akka" %% "akka-remote" % "2.3.2",
    "com.typesafe.akka" %% "akka-persistence-experimental" % "2.4-SNAPSHOT",
    "org.scalatest" %% "scalatest" % "2.1.0" % "test"
)
